#!/bin/bash
# Time: 2024-3-19 12:04:39
# Desc: 编译检查系列函数

g_cwd="$(pwd)"
g_top="$(dirname "$(realpath "$(realpath "$0")")")"
echo "g_cwd=$g_cwd, g_top=$g_top"
source "$g_top/common.sh"

if [ -f "$g_cwd/info.conf" ]; then
    source "$g_cwd/info.conf"
    msg "CI_EVENT=$CI_EVENT"
    msg "KERNEL_PATH=$KERNEL_PATH"
    msg "SHA_BEFORE=$SHA_BEFORE"
    msg "SHA_AFTER=$SHA_AFTER"
else
    err "file $g_cwd/info.conf not exist"
    return 1
fi

# Func: 编译ock
# Args: 同build_check
build_ock() {
    # shellcheck disable=SC2086
    make ARCH="$2" $3 || return 1
    if [ "$2" == "arm64" ]; then
        ./scripts/config -e CONFIG_LOCALVERSION_AUTO -d MODULE_SIG -d SECURITY_LOCKDOWN_LSM -d CPU_BIG_ENDIAN || return 1
        make -j $4 -sk ARCH="$2" CROSS_COMPILE=aarch64-linux-gnu- vmlinux modules || return 1
    else
        ./scripts/config -e CONFIG_LOCALVERSION_AUTO -d MODULE_SIG -d SECURITY_LOCKDOWN_LSM || return 1
        make -j $4 -sk ARCH="$2" vmlinux modules || return 1
    fi
}

# Func: 编译kernel
# Args: 同build_check
build_kernel() {
    case "$1" in
        ock)
            build_ock "$@" || return 1
            ;;
        *)
            err "unknown kernel version: $1"
            exit 1
            ;;
    esac
    return 0
}

# Func: 对所有commit进行编译检查
# Args:
#   $1 -- kernel version
#   $2 -- arch
#   $3 -- config
#   $4 -- make_j
build_check() {
    set -x
    local ret=0
    local commit_file="$g_cwd/commit.list"
    local nr_commit
    cd "$KERNEL_PATH" || return 1
    git log --pretty=%h -50
    git log --topo-order --pretty=%h -50

    # 遍历编译时使用topo-order更合适
    if echo "$SHA_BEFORE" | grep "^origin/"; then
        sha_before_hash="$(git log -1 --pretty='format:%h' "$SHA_BEFORE" | head -c 8)"
        msg "change $SHA_BEFORE to $sha_before_hash"
        git log --reverse --topo-order --pretty=%h "${sha_before_hash}..${SHA_AFTER}" >"$commit_file"
    elif git log --reverse --topo-order --pretty=%h "${SHA_BEFORE}..${SHA_AFTER}"; then
        git log --reverse --topo-order --pretty=%h "${SHA_BEFORE}..${SHA_AFTER}" >"$commit_file"
    elif git log --reverse --topo-order --pretty=%h "${SHA_BEFORE}..HEAD"; then
        git log --reverse --topo-order --pretty=%h "${SHA_BEFORE}..HEAD" >"$commit_file"
    else
        err "get commit list fail"
        return 1
    fi

    nr_commit=$(wc -l <"${commit_file}")
    msg "all $nr_commit commits is:"
    cat "$commit_file"

    # 遍历所有的commit并进行编译
    while read -r commit; do
        msg "try build commit $commit"
        git reset --hard "$commit"
        if build_kernel "$@"; then
            msg "commit $commit build success"
        else
            err "commit $commit build fail"
            ret=1
        fi
    done <"$commit_file"
    return $ret
}

build_check "$@"
