#!/bin/bash
# Time: 2024-05-13 15:40:15
# Desc: 流水线脚本公共函数

msg(){
    echo "$*"
}

err(){
    echo "$*" 1>&2
}

# Func: 获取流水线变量，流水线变量事先存储在文件中
# Args:
#   $1 -- var_name 流水线变量名，例如：settings.gitee_ro_key.privateKey
get_pipeline_var(){
    local var_name="$1"
    local pipeline_dir="$(realpath "./pipeline.var")"
    if [ -n "$WORKSPACE" ] && [ -d "$WORKSPACE/pipeline.var" ]; then
        pipeline_dir="$(realpath "$WORKSPACE/pipeline.var")"
    fi
    if [ -f "$pipeline_dir/$var_name" ]; then
        cat "$pipeline_dir/$var_name"
    else
        err "variable $var_name not exist"
    fi
}
