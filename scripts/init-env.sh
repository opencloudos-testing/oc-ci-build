#!/bin/bash
# Time: 2024-05-13 15:19:06
# Desc: 初始化环境
# Args: 本脚本的输入参数通过环境变量的方式提供
#   settings.gitee_ro_key.privateKey -- 访问gitee的私钥

scripts_dir="$(dirname "$(realpath "$0")")"
[ -f "$scripts_dir/common.sh" ] && source "$scripts_dir/common.sh"

date '+%Y%m%d-%H%M%S-%N'
realpath .

sed -i "/mirrors-tlinux.tencentyun.com/d" /etc/hosts
sed -i "/mirrors.tencentyun.com/d" /etc/hosts
sed -i "/mirrors.tencent.com/d" /etc/hosts

{
    echo 113.96.15.43 mirrors-tlinux.tencentyun.com
    echo 113.96.15.43 mirrors.tencentyun.com
    echo 113.96.15.43 mirrors.tencent.com
} >>/etc/hosts

yum repolist
yum install -y python3-docutils asciidoc binutils-devel dwarves elfutils-devel openssl-devel jq \
    python39 gcc-plugin-devel libcap-devel libnl3-devel llvm pciutils-devel python3-devel xmlto \
    gmp-devel elfutils-libelf-devel bison rpm-build bc gcc-c++ glibc-static hmaccalc perl-devel \
    tree rsync git perl git-lfs

echo "============= env start ============="
env
echo "============= env end ============="
# 打印常用命令和包的版本信息
for cmd in python python3 pip pip3; do
    $cmd --version
done
for pkg in glibc; do
    rpm -q $pkg
done
cat /etc/os-release
uname -a
tree -a -L 2 "$(pwd)"

# 删除历史的rpmbuild文件
rm -rf "$HOME/rpmbuild"

# 将gitee提交的信息解析并保存
info_file="$(realpath "$WORKSPACE/info.conf")"
tmp_sha_before=""
tmp_sha_after=""
kernel_path="$(realpath "$WORKSPACE/kernel")"

get_pipeline_var gitee_info >./gitee.base64
cat ./gitee.base64
cat ./gitee.base64 | base64 -d > ./gitee.json
cat ./gitee.json

ci_event="$( cat ./gitee.json | jq -r ".ci_event")"
tmp_branch="$( cat ./gitee.json | jq -r ".tmp_branch")"
tmp_ref="$( cat ./gitee.json | jq -r ".tmp_ref")"
tmp_sha_before="$( cat ./gitee.json | jq -r ".tmp_sha_before")"
tmp_sha_after="$( cat ./gitee.json | jq -r ".tmp_sha_after")"

# ci_event="$(echo "$gitee_json" | base64 -d | jq -r ".hook_name" )"
# if [ "$ci_event" = "merge_request_hooks" ]; then
#     ci_event=merge_request
#     tmp_branch="$(echo "$gitee_json" | base64 -d | jq -r ".target_branch")"
#     tmp_ref="$(echo "$gitee_json" | base64 -d | jq -r ".pull_request.merge_reference_name")"
#     tmp_sha_before="$(echo "$gitee_json" | base64 -d | jq -r ".pull_request.base.sha" | head -c 8)"
#     tmp_sha_after="$(echo "$gitee_json" | base64 -d | jq -r ".pull_request.head.sha" | head -c 8)"
# elif [ "$ci_event" = "push_hooks" ]; then
#     ci_event=push
#     tmp_branch="$(echo "$gitee_json" | base64 -d | jq -r ".ref" | sed "s|^refs/heads/||g")"
#     tmp_ref="$(echo "$gitee_json" | base64 -d | jq -r ".ref")"
#     tmp_sha_before="$(echo "$gitee_json" | base64 -d | jq -r ".before" | head -c 8)"
#     tmp_sha_after="$(echo "$gitee_json" | base64 -d | jq -r ".after" | head -c 8)"
# fi

# 如果是新分支push，就会出现sha为00000000的情况，这个时候，我们尝试将新分支最近的一个分支作为push的起点
echo "$tmp_sha_before" | grep -q "^00000000"
if [ $? -eq 0 ]; then
    tmp_sha_before="$(git log -500 --pretty=format:'%h %d' | grep "origin/" | \
        grep -v "$tmp_branch" | head -n 1 | awk '{print $1}')"
fi
{
    echo "export CI_EVENT=$ci_event"
    echo "export KERNEL_PATH='$kernel_path'"
    echo "export GIT_BRANCH='$tmp_branch'"
    echo "export GIT_REF='$tmp_ref'"
    echo "export SHA_BEFORE='$tmp_sha_before'"
    echo "export SHA_AFTER='$tmp_sha_after'"
} >"$info_file"
cat "$info_file"
