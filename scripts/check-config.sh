#!/bin/bash
# Time: 2024-05-13 16:13:57
# Desc: 检查内核config

info_conf="$WORKSPACE/info.conf"
if [ -f "$info_conf" ]; then
    cat "$info_conf"
    source "$info_conf"
else
    exit 1
fi

cd "$KERNEL_PATH"
if grep "^dist-check-diff-config:" ./dist/Makefile; then
    make dist-check-diff-config
else
    echo "the code not support dist-check-diff-config, ignore"
fi
