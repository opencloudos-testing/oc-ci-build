#!/bin/bash
# Time: 2024-05-13 15:28:19
# Desc: 下载内核代码

info_conf="$WORKSPACE/info.conf"
if [ -f "$info_conf" ]; then
    cat "$info_conf"
    source "$info_conf"
else
    exit 1
fi

kernel_path="$WORKSPACE/kernel"
# git clone --recurse-submodules git@gitee.com:OpenCloudOS/OpenCloudOS-Kernel.git "$kernel_path"
mkdir -p "$kernel_path"
# 为了加速下载内核代码，直接从本地的服务器同步内核源码文件
rsync -ap --delete 172.16.0.31::git/OpenCloudOS-Kernel/ "$kernel_path"
cd "$kernel_path"
git lfs install
git remote -v
git status
git reset --mixed HEAD
git reset --hard HEAD
git clean -fdx
git remote prune origin
git fetch --progress --prune --no-recurse-submodules origin +refs/heads/*:refs/remotes/origin/* +refs/tags/*:refs/tags/*

git lfs pull

info_conf="$WORKSPACE/info.conf"
if [ -f "$info_conf" ]; then
    cat "$info_conf"
    source "$info_conf"
else
    exit 1
fi

cd "$KERNEL_PATH" && pwd
# 先将代码分支切换到目标分支
git remote -v
git branch
git clean -q -fdx
git checkout --force -B "$GIT_BRANCH" "origin/$GIT_BRANCH"
git pull || exit 1
git lfs pull || exit 1
# 如果是MR，那么还需要将代码预合入
if [ "$CI_EVENT" == "merge_request" ]; then
    git fetch origin "${GIT_REF}:${GIT_REF}" || exit 1
    git merge "$GIT_REF" || exit 1
fi
git log -50 --pretty='format:%H|%cn|%ct|%cd|%an|%s'

