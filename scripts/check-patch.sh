#!/bin/bash
# Time: 2024-05-13 16:13:57
# Desc: 对内核代码进行scripts/checkpatch.pl检查

info_conf="$WORKSPACE/info.conf"
if [ -f "$info_conf" ]; then
    cat "$info_conf"
    source "$info_conf"
else
    exit 1
fi

python3 -m pip install ply gitpython
cd "$KERNEL_PATH"
if [[ -z "$SHA_BEFORE" ]]; then
    ./scripts/checkpatch.pl --git "$SHA_AFTER" 2>&1 | tee ./check-patch.log
else
    ./scripts/checkpatch.pl --git "$SHA_BEFORE".."$SHA_AFTER" 2>&1 | tee ./check-patch.log
fi
if grep "^total:.*line.* checked" ./check-patch.log; then
    echo "check patch run normal"
else
    echo "check patch run fail"
    exit 1
fi
if grep "^ERROR:" ./check-patch.log; then
    echo "check patch fail"
    exit 1
else
    echo "check patch success"
fi
